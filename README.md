**TestNG(Test Next Generation)**

TestNG  is an open source test automation framework designed to simplify testing needs.

Using TestNG we can,

1.Validate the response(Assert class)

2.Test Automation Framework (Orchestration of our testclass using Annotation)

**Annotation** – It’s a method of metadata that can be added to java source code.

Different types of annotation are there,in my framework I have used @ Test,@ BeforeTest and @ AfterTest

Structure of  framework

**1.Framework design type**:

Using TestNG.xml file which can run and execute the tetscases by,

**a)Serial Execution** - Multiple testcases are executed one after another.

**b)Parallel Execution**- Multiple testcases are executed simultaneously based on the given thread count which signifies how many test cases should run per execution.

c)By adding **Maven surefire Plugin** in dependencies, we can run our test cases in cmd without IDE.

**2.TestClass:**

In API Package, For each API’s  I have separate testclass and multiple testcases for each testclass.

**@ Test** - defines test method

**3.Common method components:**

For configuring and triggering the API using 
Given(), When() and Then()

**i) API components:**

Endpoint,Request and common functions to trigger API and extract statuscode and responsebody

**ii)Utility function Package:**

a)To get test log files, we are creating directory,if it is not exists.

If directory exists,we are deleting and creating it.

b) To create logfiles and the endpoint,requestbody,reponsebody and its respective headers added into notepad file of each testclass

c)Excel data extractor is to read the test datas from an excel file

d)ExtentListener class – In this class I had implemented ITestListener and used its own methods.

By using **listeners tag** in xml,will track the test execution such as starting of testcase,testcase failure etc.,

**4.Test Data File**

It consists of  requestbody input datas entered in excel file.

**PARAMETERIZATION**

a)By **@DataProvider** – To run test cases with multiple input datas.

To pass testdata,declare a method that runs data  in form of two-dimensional object array[].

b)By passing  **@ Parameters** {(“a”,”b”)} in test method

**Parameter tags** in testing.xml file we can pass the input data

**5.Libraries:**

Below given are the dependencies added and handled by Maven repository

RestAssured – To trigger API and extract statuscode and responsebody

TestNG – To validate the reponse body using Assert class, organise and  run testcases through testNG

JsonPath – To parse the response

Apache.poi – To read data from an excel file

Extentreport

**6.Reports:**

**************

Extent report – User friendly and widely used








