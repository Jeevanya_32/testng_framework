package API_package;
import API_common_methods.common_method_handle_API;
import apirequest_repository.PostAPI_request_repository;
import apirequest_repository.PutAPI_request_repository;
import common_methods_utility.Directory_handle;
import common_methods_utility.handle_API_logs;
import endpoint_package.Post_APIendpoint;
import endpoint_package.Put_APIendpoint;
import io.restassured.path.json.JsonPath;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class testclass_put extends common_method_handle_API {
	static File log_dirname;
	static String requestbody; 
	static String endpoint ;
	static String responsebody;
	@BeforeTest
	public static void Test_Setup() throws IOException {
		log_dirname=Directory_handle.log_directory_creation("API_putlogs");
	     requestbody = PutAPI_request_repository.put_req();
         endpoint = Put_APIendpoint.endpoint_put();
	}
	@Test(description = "executing the PUT API and validating the responsebody")
public static void executor_put() throws IOException {
	
        
        for (int i=0; i<5; i++) {
           int statuscode = put_statuscode(requestbody,endpoint);
           System.out.println(statuscode);
        if (statuscode ==200) {
           responsebody = put_responsebody(requestbody,endpoint);
           System.out.println(responsebody);
          
           testclass_put.validator(requestbody,responsebody);
           break;
}
           else
           {
        	   System.out.println("Incorrect status code is found hence retry");
           }
}
}
public static void validator(String requestbody,String responsebody) {
	JsonPath jsp_req=new JsonPath(requestbody);
	String req_name=jsp_req.getString("name");
	String req_job=jsp_req.getString("job");

	JsonPath jsp_res=new JsonPath(responsebody);
	String res_name=jsp_res.getString("name");
	String res_job=jsp_res.getString("job");
	LocalDateTime currentdate = LocalDateTime.now();
	String expected_date=currentdate.toString().substring(0,11);
	String res_updatedat=jsp_res.getString("updatedAt");
	res_updatedat=res_updatedat.substring(0,11);

	Assert.assertEquals(res_name,req_name);
	Assert.assertEquals(res_job,req_job);
	Assert.assertEquals(res_updatedat,expected_date);
}
@AfterTest
public static void Test_teardown() throws IOException {
	String testclass_name=testclass_put.class.getName();
	 handle_API_logs.evidence_creator(log_dirname,"testclass_put",endpoint, requestbody, responsebody);
}
}