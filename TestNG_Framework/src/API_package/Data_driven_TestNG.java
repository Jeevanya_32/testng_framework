package API_package;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import API_common_methods.common_method_handle_API;
import apirequest_repository.Data_provider_TestNG;
import apirequest_repository.PostAPI_request_repository;
import common_methods_utility.Directory_handle;
import common_methods_utility.handle_API_logs;
import endpoint_package.Post_APIendpoint;
import io.restassured.path.json.JsonPath;

public class Data_driven_TestNG extends common_method_handle_API{
	static File log_dirname;
	static String requestbody;
	static String endpoint;
	static String responsebody;
	
	@DataProvider()
	public Object[] [] post_requestbody(){
		return new Object[][]
				{
			        {"Morpheus","Leader"},
			        {"Ragu","Manager"},
			        {"Jas","QA"}
				};
	}
	@BeforeTest
	public static void Test_Setup() throws IOException {
		log_dirname = Directory_handle.log_directory_creation("API_postlogs");
		
		endpoint = Post_APIendpoint.endpoint_post();
	}

	//@Test (dataProvider = "post_requestbody")
	@Test (dataProvider = "post_data", dataProviderClass =Data_provider_TestNG.class)
	public static void executor_post(String name,String job ) throws IOException {
		requestbody = "{\r\n"
				+ "    \"name\": \""+name+"\",\r\n"
				+ "    \"job\": \""+job+"\"\r\n"
				+ "}";
		for (int i = 0; i < 5; i++) {
			int statuscode = post_statuscode(requestbody, endpoint);
			System.out.println(statuscode);

			if (statuscode == 201) {
				responsebody = post_responsebody(requestbody, endpoint);
				System.out.println(responsebody);
				testclass_post.validator(requestbody, responsebody);
				break;
			} 
			else {
				System.out.println("Incorrect statuscode is found hence retry");
			}
		}
	}

	public static void validator(String requestbody, String responsebody) {

		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		JsonPath jsp_res = new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_id = jsp_res.getString("id");
		LocalDateTime currentdate = LocalDateTime.now();
		String expected_date = currentdate.toString().substring(0, 11);
		String res_createdat = jsp_res.getString("createdAt");
		res_createdat = res_createdat.substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdat, expected_date);
	}

	@AfterTest
	public static void Test_teardown() throws IOException {
		String testcase_name = testclass_post.class.getName();
		handle_API_logs.evidence_creator(log_dirname, testcase_name, endpoint, requestbody, responsebody);
	}

}
