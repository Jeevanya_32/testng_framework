package apirequest_repository;

import java.io.IOException;
import java.util.ArrayList;

import common_methods_utility.Excel_data_extractor;

public class PutAPI_request_repository {
public static String put_req() throws IOException{
	ArrayList<String> data=Excel_data_extractor.Excel_data_reader("TestdataAPI","Put_API","Put_TC1");
	//System.out.println(data);
	String name=data.get(1);
	String job=data.get(2);
	String requestbody="{\r\n"
            + "    \"name\": \""+name+"\",\r\n"
            + "    \"job\": \""+job+"\"\r\n"
            + "}";
     return requestbody;
  }
}
