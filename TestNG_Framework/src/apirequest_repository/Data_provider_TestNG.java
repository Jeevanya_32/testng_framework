package apirequest_repository;

import org.testng.annotations.DataProvider;

public class Data_provider_TestNG {
	@DataProvider()
	public Object[] [] post_data(){
		return new Object[][]
				{
			        {"Mahi","SrMgr"},
			        {"Ragu","Manager"},
			        {"Jas","SrQA"}
				};
	}
}
